document.addEventListener("DOMContentLoaded", function() {
    function createParagraph() {
        let elmentPCreated = document.createElement('p');
        elmentPCreated.textContent = 'You clicked the button!';
        document.body.appendChild(elmentPCreated);
    }

    const buttons = document.querySelectorAll('button');

    for (let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', createParagraph);
    }
});